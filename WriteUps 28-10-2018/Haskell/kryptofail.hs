import Data.Char

main = do putStrLn "Escriba una palabra"
          word <- getLine
          putStrLn "Escriba: encode - decode"
          code <- getLine
          putStrLn "número de desplazamientos, decode (-num)"
          numero <- getLine
          let salida = encode word (read numero :: Int)
          putStrLn $ code ++ " word: " ++ salida



xEnc x y = ['a'..'z'] !! (((x + y) `mod` 26 ))

encode :: [String] -> Int -> String
encode [] y = ""
encode (x:xs) y = (xEnc ((ord x)-97) y):(encode xs y)