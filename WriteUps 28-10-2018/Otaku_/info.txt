Titulo: Otaku?

Descripcion: Demuestra tus habilidades consiguiendo este reto.

Pista: Algo no funciona como deberia.

Flag: HM{a377efcf245b1ed11be9ccba2f7cc571}

Writeup: Primero hay que darse cuenta que es una imagen, para eso hay un
string JPEG al final de la foto. Habrá que arreglar la cabecera y ver
la foto, que será erza. Ahora habrá que sacar el zip y descomprimirlo,
los dos con la pass erza. Por ultimo habrá que descomprimir el archivo
xajmiga.enc (xajmiga es saga en Somalí) con un diccionario de las sagas
de la serie en mayúsculas y cifrado aes256 con la pass EDOLAS.

